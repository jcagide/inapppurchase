//
//  ViewController.swift
//  InAppPurchase
//
//

import UIKit
import StoreKit

class ViewController: ActionViewController {
   
    @IBOutlet var accessContentButton: UIButton!
    var specialContent:ContentViewController!
    var subcriptionView:SubcriptionViewController!

    @IBAction func cmdAccesSubscriptionContent(){
        self.addActivityView(self.accessContentButton)
        SubscriptionManager.sharedInstance.validateSubscription({ (isSubscribed, error)->Void in
            
            dispatch_async(dispatch_get_main_queue(), {
                
                if(error==nil){
                    if(!isSubscribed){
                        self.modalSubcriptionView()
                    }else{
                        self.modalSpecialContent()
                    }
                    self.removeActivityView(self.accessContentButton)
                }else{
                    self.showAlertView("Oh Oh! ", message:error!)
                    self.removeActivityView(self.accessContentButton)
                }
            })
        })
    }
    
    func modalSpecialContent(){
        self.specialContent = ContentViewController()
        self.presentViewController(specialContent, animated: true, completion: nil)
    }

    func modalSubcriptionView(){
        self.subcriptionView = SubcriptionViewController()
        self.presentViewController(subcriptionView, animated: true, completion: nil)
    }

    
}

