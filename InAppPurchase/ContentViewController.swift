//
//  ContentViewController.swift
//  InAppPurchase
//
//  Created by Jesus Cagide on 10/2/15.
//  Copyright © 2015 Brian Coleman. All rights reserved.
//

import UIKit

class ContentViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func cmdClose(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
