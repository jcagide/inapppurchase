//
//  SubcriptionViewController.swift
//  InAppPurchase
//
//  Created by Jesus Cagide on 9/25/15.
//

import UIKit

class SubcriptionViewController: ActionViewController {
    
    @IBOutlet weak var acceptButton : UIButton!
    @IBOutlet weak var restoreButton : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.acceptButton.layer.cornerRadius =  20
        
        self.acceptButton.addTarget(self, action: Selector("cmdAcceptButton"), forControlEvents: UIControlEvents.TouchUpInside)
        
        //MARK- Subscription experiment
        
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "onSuccessfullySubscribedNotification:",
            name: successfullySubscribedNotification,
            object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "onErrorInSubscriptionNotification:",
            name: errorInSubscriptionNotification,
            object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "onSuccessfullyRestoredNotification:",
            name: successfullyRestoredNotification,
            object: nil)
        

        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "onSubscriptionDateExpiredNotifcation:",
            name: subscriptionDateExpiredNotifcation,
            object: nil)
        
    }
    
    deinit{
         NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
    @objc func onSubscriptionDateExpiredNotifcation(notification: NSNotification){
        self.removeActivityView(self.acceptButton)
        self.showAlertView("Oh oh", message:"The shared secret you provided does not match the shared secret on file for your account.")
    }
    
    @objc func onSuccessfullySubscribedNotification(notification: NSNotification){
        self.removeActivityView(self.acceptButton)
        self.cmdCloseView()
        print("SuccessfullySubscribedNotification")
    }
    
    @objc func onErrorInSubscriptionNotification(notification: NSNotification){
        
        self.removeActivityView(self.acceptButton)
        let error =  SubscriptionManager.sharedInstance.error
        self.showAlertView("Oh oh", message:error)
    }
    
    @objc func onSuccessfullyRestoredNotification(notification: NSNotification){
        
        self.removeActivityView(self.acceptButton)
        self.cmdCloseView()
        print("successfullyRestoredNotification")
    }

    @IBAction func cmdCloseView(){
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func cmdRestore(){
        self.addActivityView(self.acceptButton)
        SubscriptionManager.sharedInstance.begingRestoreTransactions()
    }
    
    func cmdAcceptButton(){
        self.addActivityView(self.acceptButton)
        SubscriptionManager.sharedInstance.beginPurchase()
    }
    
}
