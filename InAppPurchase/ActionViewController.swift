//
//  ActionViewController.swift
//  InAppPurchase
//
//  Created by Jesus Cagide on 10/2/15.
//

import UIKit

class ActionViewController: UIViewController {

    var activityIndicator : UIActivityIndicatorView!
    
    func addActivityView(button:UIButton){
        
        self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        
        self.activityIndicator.center = CGPointMake(button.bounds.width/2, button.bounds.height/2)
        button.setTitle("", forState: UIControlState.Normal)
        button.addSubview(self.activityIndicator)
        self.activityIndicator.startAnimating()
        button.enabled = false
    }
    
    func removeActivityView(button:UIButton){
        
        if(self.activityIndicator != nil){
            button.enabled = true
            self.activityIndicator.stopAnimating()
            self.activityIndicator.removeFromSuperview()
            button.setTitle("Access", forState: UIControlState.Normal)
        }
    }
    
    func showAlertView(title:String, message:String)->Void{
        
        let alertController = UIAlertController(title: title, message:message, preferredStyle: .Alert)
        
        let OKAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { _ in
            alertController.dismissViewControllerAnimated(true, completion: nil)
        }
        alertController.addAction(OKAction)
        self.presentViewController(alertController, animated: true){}
    }

}
