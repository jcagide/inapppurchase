//
//  AppDelegate.swift
//  InAppPurchase
//


import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var isFirstLaunched = false

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        Parse.setApplicationId("Kg2dStdsO0nUcqdo8ksnRiJtG7vbVJCT7CKpPLZi", clientKey: "whN7ZguaDzMAVpe4i7SjykJ3jBxbeiUhlqe6JtTJ")
        
    
        // Check first launch
        if !NSUserDefaults.standardUserDefaults().boolForKey("HasLaunchedOnce"){
            isFirstLaunched = true
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "HasLaunchedOnce")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    
    
        PFUser.enableAutomaticUser()
        
        // have to save synchronously to avoid query anomonous user error
        if PFAnonymousUtils.isLinkedWithUser(PFUser.currentUser()) && isFirstLaunched{
            do {
                try PFUser.currentUser()?.save()
            } catch _ {
                print("error")
            }
        }
        
        let defaultACL = PFACL();
        
                defaultACL.setPublicReadAccess(true)
        
        PFACL.setDefaultACL(defaultACL, withAccessForCurrentUser:true)


        SubscriptionManager.sharedInstance
        
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

