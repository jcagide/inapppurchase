//
//  SubscriptionManager.swift
//  InAppPurchase
//
//  Created by Jesus Cagide on 9/25/15.
//

let successfullySubscribedNotification = "co.inapp.successfullySubscribed"
let subscriptionDateExpiredNotifcation = "co.inapp.subscriptionDateExpire"
let successfullyRestoredNotification = "co.inapp.successfullyRestored"
let errorInSubscriptionNotification = "co.inapp.subscriptionError"

let secretKey = "fc55f31343904d29a497ef049df2f7b6"
let storeURL = "https://sandbox.itunes.apple.com/verifyReceipt"

import UIKit

class SubscriptionManager: NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {

    var isRefreshingReceipt = false
    let productID = "com.vimo.inapprage.product1"
    let productsID  = Set(["com.vimo.inapprage.product1"])
    var productsRequest:SKProductsRequest = SKProductsRequest()
    var expirationDate:NSDate?
    var products = [String : SKProduct]()
    var error:String = "No error yet"
    var validationCallBack : ((isSubscribed:Bool, error:String?)->Void)!
    
    static let sharedInstance : SubscriptionManager  = SubscriptionManager()
    
    override init(){
        super.init()
        
        self.requestProductsWithProductIdentifiers()
    }
    
    // asign Value
    func validateSubscription(callback:(isSubscribed:Bool, error:String?)->Void )->Void{
        
        self.validationCallBack = callback
        if let subscriptionDate = getActualSubscriptionDate(){
            self.validateSubscriptionDate(subscriptionDate )
        }else{
            self.validationCallBack(isSubscribed: false, error: nil)
            print("no subcription")
        }
    }
    
    func getActualSubscriptionDate()->NSDate?{
        
        self.expirationDate = (PFUser.currentUser()!["subscriptionExpirationDate"] as? NSDate?)!
        return self.expirationDate
    }
    
    func validateSubscriptionDate(subcriptionDate:NSDate )->Void{
        
        let today = NSDate()
        
        switch today.compare(subcriptionDate){
            
        case NSComparisonResult.OrderedDescending:
            self.receiptValidation()
            print("checking the server ...");
            break
            
        case NSComparisonResult.OrderedAscending:
            self.validationCallBack(isSubscribed: true, error:nil)
            print("All ok today \(today) is before subscription : \(subcriptionDate) ");
            break
            
        default:
            self.receiptValidation()
            print("checking the server ...coz default");
            break
        }
    }
    
    func requestProductsWithProductIdentifiers(){
        
        products.removeAll()
        self.productsRequest = SKProductsRequest(productIdentifiers: self.productsID)
        self.productsRequest.delegate = self;
        
        dispatch_async(dispatch_get_global_queue(0, 0), {
            self.productsRequest.start()
        })
    }
    
    func requestDidFinish(request: SKRequest) {
        
        if let appStoreReceiptURL = self.getAppStoreReceipt() {
            
            if !isRefreshingReceipt {
                isRefreshingReceipt = true
                let request = SKReceiptRefreshRequest(receiptProperties: nil)
                request.delegate = self
                request.start()
            }
        }
    }
    
    
    //MARK- SKProductsRequestDelegate
    func productsRequest(request: SKProductsRequest, didReceiveResponse response: SKProductsResponse){
        let productsFromResponse = response.products
        for product in productsFromResponse {
            products[product.productIdentifier] = product
        }
    }
    
    func beginPurchase() {
        
        if SKPaymentQueue.canMakePayments() {
            
            if let product: SKProduct = products[productID] {
                
                let payment = SKPayment(product: product)
                SKPaymentQueue.defaultQueue().addTransactionObserver(self)
                SKPaymentQueue.defaultQueue().addPayment(payment);
                
            } else {
                error = "Product \(productID) not found"
                NSNotificationCenter.defaultCenter().postNotificationName(errorInSubscriptionNotification, object: self)
                
            }
        } else {
            error =  "User cannot make payments"
            NSNotificationCenter.defaultCenter().postNotificationName(errorInSubscriptionNotification, object: self)
        }
    }
    
    // MARK- SKPaymentTransactionObserver
    func paymentQueue(queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        for transaction in transactions {
            switch transaction.transactionState{
                
            case SKPaymentTransactionState.Purchased:
                purchased(transaction)
                break
                
            case SKPaymentTransactionState.Failed:
                failed(transaction)
                break
                
            case SKPaymentTransactionState.Restored:
                restored(transaction)
                break
                
            default:
                break
            }
        }
    }
    
    
    func restored(transaction : SKPaymentTransaction){
    }
    
    func failed(transaction : SKPaymentTransaction){
        
        if let _ = transaction.error {
            error = transaction.error?.localizedDescription ?? "failed transaccion"
            NSNotificationCenter.defaultCenter().postNotificationName(errorInSubscriptionNotification, object: self)
        }
        
        SKPaymentQueue.defaultQueue().finishTransaction(transaction)
        SKPaymentQueue.defaultQueue().removeTransactionObserver(self)
    }
    
    func purchased(transaction : SKPaymentTransaction){
        
        SKPaymentQueue.defaultQueue().finishTransaction(transaction)
        
        if let receiptURL = self.getAppStoreReceipt()
        {
            self.receiptValidation()
        } else {
            self.refreshReceipt()
        }
    }
    
    func refreshReceipt(){
        
        if !isRefreshingReceipt {
            isRefreshingReceipt = true
            let request = SKReceiptRefreshRequest(receiptProperties: nil)
            request.delegate = self
            request.start()
            
            dispatch_async(dispatch_get_main_queue(), {
                NSNotificationCenter.defaultCenter().postNotificationName(successfullySubscribedNotification, object: self)
            })
        }
    }
    
    
    func getAppStoreReceipt()-> NSURL?{
        
        if let appStoreReceiptURL =
            NSBundle.mainBundle().appStoreReceiptURL where
            NSFileManager.defaultManager().fileExistsAtPath(appStoreReceiptURL.path!){
                return appStoreReceiptURL
        }
        return nil
    }
    
    
    func jsonHandler(data: NSData?, response: NSURLResponse?, error: NSError?)->(){
        
        if(error == nil && data != nil){
            do {
                if let jsonResponse: NSDictionary =
                    try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as? NSDictionary {
                        
                        let expirationDate =
                        self.expirationDateFromResponse(jsonResponse)
                        self.updateIAPExpirationDate(expirationDate)
                }
            }
            catch {
                self.validationCallBack(isSubscribed:false, error: "Network error")
            }
        }else{
            
            self.validationCallBack(isSubscribed:false, error: error!.localizedDescription)
            print("error: \(error)")
        }
    }
    
    //MARK- receiptValidation I need to add receiptValidation in only one thread
    
    func receiptValidation(){
        
        if let receiptPath = self.getAppStoreReceipt() {
            
            let receiptData = NSData(contentsOfURL:receiptPath)
            
            let receiptDictionary = ["receipt-data" : receiptData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0)), "password" : secretKey]
            
            do {
                
                let requestData  =  try NSJSONSerialization.dataWithJSONObject(receiptDictionary, options: NSJSONWritingOptions(rawValue: 0)) as NSData
                
                let storeURL = NSURL(string:"https://sandbox.itunes.apple.com/verifyReceipt")!
                
                let storeRequest = NSMutableURLRequest(URL: storeURL)
                storeRequest.HTTPMethod = "POST"
                storeRequest.HTTPBody = requestData
                
                let session = NSURLSession(configuration:NSURLSessionConfiguration.defaultSessionConfiguration())
                
                let task = session.dataTaskWithRequest(storeRequest, completionHandler:jsonHandler)
                task.resume()
            }
            catch {
                self.validationCallBack(isSubscribed:false, error:"Error on device validation")
                print("NSJSONSerialization fail")
            }
        }
    }
    
    func updateIAPExpirationDate(date : NSDate?) {
        
        let today = NSDate()
        
        if let newSubscriptionDate =  date {
            
            switch today.compare(newSubscriptionDate){
                
            case NSComparisonResult.OrderedDescending:
                self.validationCallBack(isSubscribed:false, error: nil)
                print("subscriptionDateExpiredNotifcation \(newSubscriptionDate)")
                NSNotificationCenter.defaultCenter().postNotificationName(subscriptionDateExpiredNotifcation, object: self)
                break
            case NSComparisonResult.OrderedAscending:
                self.updateSubscriptionExpirationDate(newSubscriptionDate)
                print("All ok today \(today) is before subscription : \(newSubscriptionDate) ");
                break
                
            default:
                self.updateSubscriptionExpirationDate(newSubscriptionDate)
                print("All ok today \(today) is before subscription : \(newSubscriptionDate) ");
                break
            }
        }else{
            self.validationCallBack(isSubscribed:false, error:nil)
            NSNotificationCenter.defaultCenter().postNotificationName(subscriptionDateExpiredNotifcation, object: self)
        }
    }
    
    func expirationDateFromResponse(jsonResponse: NSDictionary) -> NSDate?{
        
        if let receiptInfo: NSArray = jsonResponse["latest_receipt_info"] as? NSArray {
            
            let lastReceipt = receiptInfo.lastObject as! NSDictionary
            let formatter = NSDateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
            
            let expirationDate: NSDate =
            formatter.dateFromString(lastReceipt["expires_date"] as! String) as NSDate!
            return expirationDate
        } else {
            return nil
        }
    }
    
    func updateSubscriptionExpirationDate(date:NSDate){
        
        self.expirationDate  =  date
        
        dispatch_async(dispatch_get_main_queue(), {
            NSNotificationCenter.defaultCenter().postNotificationName(successfullySubscribedNotification, object: self)
        })
        
        if let user = PFUser.currentUser() {
            user["subscriptionExpirationDate"] = date
            user.saveEventually({ (result, error) -> Void in
                if error == nil {
                    print("subscriptionExpirationDate is in the server? : \(result)")
                }else{
                    print(error)
                }
            })
        }
    }
    
    func begingRestoreTransactions(){
        
        if !self.isRefreshingReceipt {
            
            self.isRefreshingReceipt = true
            let request = SKReceiptRefreshRequest(receiptProperties: nil)
            request.delegate = self
            request.start()
        }
        SKPaymentQueue.defaultQueue().addTransactionObserver(self)
        SKPaymentQueue.defaultQueue().restoreCompletedTransactions()
    }
    
    func paymentQueue( queue: SKPaymentQueue,
        restoreCompletedTransactionsFailedWithError error: NSError){
            self.error = error.localizedDescription
            NSNotificationCenter.defaultCenter().postNotificationName(errorInSubscriptionNotification, object: self)
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(queue: SKPaymentQueue) {
        
        if let appStoreReceipt = self.getAppStoreReceipt(){
            
            let lastValidationCallback = self.validationCallBack
            self.validationCallBack = { (isSubscribed, error) -> Void in
                dispatch_async(dispatch_get_main_queue(), {
                    if(!isSubscribed ){
                        self.validationCallBack = lastValidationCallback
                        self.error = error ?? "No purchases registered or your subscription expired"
                        NSNotificationCenter.defaultCenter().postNotificationName(errorInSubscriptionNotification, object: self)
                    }
                })
            }
            self.receiptValidation()
        }else{
            self.error = "No purchases registered"
            NSNotificationCenter.defaultCenter().postNotificationName(errorInSubscriptionNotification, object: self)
        }
    }
    
}
